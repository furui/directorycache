package directorycache

import (
	"io"
	"os"
	"sync"
)

// EntryState stores the current state of the cache entry
type EntryState int

const (
	// EntryStateNotCached is the initial cache state of an entry
	EntryStateNotCached EntryState = iota
	// EntryStateCaching is the state where caching is in progress
	EntryStateCaching
	// EntryStateCached is the state where caching has stopped
	EntryStateCached
	// EntryStateReading is the state where file is being read and also being cached
	EntryStateReading
	// EntryStateError is the state when the file has run into an error
	EntryStateError
)

// File stores a description of the file
type File struct {
	Path        string
	Info        os.FileInfo
	Buffer      []byte
	State       EntryState
	readPointer int
	mutex       sync.Mutex
}

// UpdateInfo stats the file and checks if the info still matches
func (f *File) UpdateInfo() (bool, error) {
	s, err := os.Stat(f.Path)
	if err != nil {
		return true, err
	}
	if s.ModTime() == f.Info.ModTime() && s.Size() == f.Info.Size() {
		return false, nil
	}
	f.Info = s
	return true, nil
}

// DoCache reads the file into the cache
func (f *File) DoCache() error {
	file, err := os.Open(f.Path)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.Seek(int64(len(f.Buffer)), 0)
	if err != nil {
		return err
	}
	buf := make([]byte, 32767)
	n, err := file.Read(buf)
	if err != nil {
		return err
	}
	f.Buffer = append(f.Buffer, buf[:n]...)
	return nil
}

// Read reads from the file or the cache
func (f *File) Read(b []byte) (n int, err error) {
	err = nil
	n = 0
	f.mutex.Lock()
	defer f.mutex.Unlock()
	if len(f.Buffer) > len(b)+f.readPointer {
		// is the cache length bigger than what we are attempting to read, then just return that into the buffer
		n = copy(b, f.Buffer[f.readPointer:f.readPointer+len(b)])
		f.readPointer += n
	} else if len(f.Buffer) > f.readPointer {
		// is the cache length bigger than the read pointer, then measure the excess cache, and read from the file
		excessCache := len(f.Buffer) - f.readPointer
		file, err := os.Open(f.Path)
		if err != nil {
			return n, err
		}
		defer file.Close()
		_, err = file.Seek(int64(len(f.Buffer)), 0)
		if err != nil {
			return n, err
		}
		buf := make([]byte, len(b)-excessCache)
		n2, err := file.Read(buf)
		if err != nil && err != io.EOF {
			return n, err
		}
		f.Buffer = append(f.Buffer, buf[:n2]...)
		n = copy(b, f.Buffer[f.readPointer:excessCache+n2])
		f.readPointer += excessCache + n2
	} else {
		// otherwise just read and update the cache
		file, err := os.Open(f.Path)
		if err != nil {
			return n, err
		}
		defer file.Close()
		_, err = file.Seek(int64(f.readPointer), 0)
		if err != nil {
			return n, err
		}
		buf := make([]byte, len(b))
		n, err = file.Read(buf)
		if err != nil && err != io.EOF {
			return n, err
		}
		f.Buffer = append(f.Buffer, buf[:n]...)
		n2 := copy(b, buf)
		if n != n2 {
			panic("bytes read and bytes copied differed")
		}
		f.readPointer += n
	}
	if int64(f.readPointer) >= f.Info.Size() {
		err = io.EOF
	}
	return
}
