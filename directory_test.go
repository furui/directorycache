package directorycache

import (
	"bytes"
	"io/ioutil"
	"os"
	"testing"
)

func TestDirectoryRead(t *testing.T) {
	d, err := NewDirectory("testing/american", 10000, 0.8, true)
	if err != nil {
		t.Error(err)
	}
	defer d.Close()

	f := d.GetCurrentFile()
	if f.Info.Name() != "american" {
		t.Error("Incorrect filename found, expected \"american\" got: ", f.Info.Name())
	}

}

func TestDirectoryNextPrev(t *testing.T) {
	d, err := NewDirectory("testing/american", 10000, 0.8, true)
	if err != nil {
		t.Error(err)
	}
	defer d.Close()

	g := d.NextFile()
	f := d.GetCurrentFile()
	if f.Info.Name() != "colby" {
		t.Error("Incorrect filename found, expected \"colby\" got: ", f.Info.Name())
	}
	if f != g.Value {
		t.Error("Pointers don't match")
	}

	g = d.PrevFile()
	f = d.GetCurrentFile()
	if f.Info.Name() != "american" {
		t.Error("Incorrect filename found, expected \"american\" got: ", f.Info.Name())
	}
	if f != g.Value {
		t.Error("Pointers don't match")
	}
}

func setupDirectories() {
	if _, err := os.Stat("testing/testdir"); os.IsNotExist(err) {
		os.MkdirAll("testing/testdir", 0777)
		ioutil.WriteFile("testing/american", bytes.Repeat([]byte{'-'}, 2000), 0777)
		ioutil.WriteFile("testing/swiss", bytes.Repeat([]byte{'-'}, 2500), 0777)
		ioutil.WriteFile("testing/colby", bytes.Repeat([]byte{'-'}, 5000), 0777)
		ioutil.WriteFile("testing/jack", bytes.Repeat([]byte{'-'}, 1000), 0777)
		ioutil.WriteFile("testing/muenster", bytes.Repeat([]byte{'-'}, 3000), 0777)
	}
}

func TestMain(m *testing.M) {
	setupDirectories()
	r := m.Run()

	os.Exit(r)
}
