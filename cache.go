package directorycache

import (
	"container/list"
	"io"
	"sync"
)

// Cache simultaneously caches and provides IO readers
type Cache struct {
	entries           *list.List
	currentEntry      *list.Element
	MaxSize           int64
	ForwardPercentage float64
	direction         Direction
	circular          bool
	mutex             sync.Mutex
	forwardPointer    *list.Element
	backwardPointer   *list.Element
	stopCaching       chan bool
	caching           bool
}

// SetEntries sets the entries that the cache is concerned with
func (c *Cache) SetEntries(entries *list.List) {
	c.entries = entries
}

// SetCurrentEntry sets the entries that the cache is concerned with and begins caching
func (c *Cache) SetCurrentEntry(entry *list.Element) {
	c.currentEntry = entry
	if c.currentEntry != nil {
		c.StopCaching()
		c.StartCaching()
	}
}

// SetCircular sets if the cache is circular or not
func (c *Cache) SetCircular(circular bool) {
	c.circular = circular
}

// SetDirection sets the direction we are going
func (c *Cache) SetDirection(direction Direction) {
	c.direction = direction
}

func (c *Cache) getNext(e *list.Element) *list.Element {
	var n *list.Element
	if c.direction == DirectionForward {
		n = e.Next()
		if n == nil && c.circular == true {
			n = c.entries.Front()
		}
	} else {
		n = e.Prev()
		if n == nil && c.circular == true {
			n = c.entries.Back()
		}
	}
	if n == c.currentEntry {
		n = nil
	}
	return n
}

func (c *Cache) getPrev(e *list.Element) *list.Element {
	var n *list.Element
	if c.direction == DirectionForward {
		n = e.Prev()
		if n == nil && c.circular == true {
			n = c.entries.Back()
		}
	} else {
		n = e.Next()
		if n == nil && c.circular == true {
			n = c.entries.Front()
		}
	}
	if n == c.currentEntry {
		n = nil
	}
	return n
}

func (c *Cache) calculateCacheMaxSize() (forward int64, backward int64) {
	return int64(float64(c.MaxSize) * c.ForwardPercentage), int64(float64(c.MaxSize) * (1.0 - c.ForwardPercentage))
}

func (c *Cache) doCache(d Direction) {
	var p *list.Element
	if d == DirectionForward {
		p = c.forwardPointer
	} else {
		p = c.backwardPointer
	}
	if p == nil {
		return
	}
	f := p.Value.(*File)
	f.mutex.Lock()
	if f.State == EntryStateNotCached {
		f.readPointer = 0
		f.State = EntryStateCaching
		f.Buffer = []byte{}
	} else if f.State == EntryStateCaching {
		update, err := f.UpdateInfo()
		if err != nil {
			f.State = EntryStateError
			f.mutex.Unlock()
			return
		}
		if update == true {
			f.State = EntryStateNotCached
		} else {
			err := f.DoCache()
			if err == io.EOF {
				f.State = EntryStateCached
			} else if err != nil {
				f.State = EntryStateError
			}
		}
	} else if f.State == EntryStateCached {
		update, err := f.UpdateInfo()
		if err != nil {
			f.State = EntryStateError
			f.mutex.Unlock()
			return
		}
		if update == true {
			f.State = EntryStateNotCached
			f.mutex.Unlock()
			return
		}
		if d == DirectionForward {
			c.forwardPointer = c.getNext(c.forwardPointer)
		} else {
			c.backwardPointer = c.getPrev(c.backwardPointer)
		}
	} else {
		if d == DirectionForward {
			c.forwardPointer = c.getNext(c.forwardPointer)
		} else {
			c.backwardPointer = c.getPrev(c.backwardPointer)
		}
	}
	f.mutex.Unlock()
}

// StopCaching stops caching
func (c *Cache) StopCaching() {
	if c.caching == true {
		c.caching = false
		c.stopCaching <- true
	}
}

// StartCaching starts caching
func (c *Cache) StartCaching() {
	c.forwardPointer = c.currentEntry
	c.backwardPointer = c.currentEntry
	c.caching = true
	go func(c *Cache) {
		for {
			select {
			case _ = <-c.stopCaching:
				return
			default:
				forwardMax, backwardMax := c.calculateCacheMaxSize()
				forward := c.GetForwardCachedSize()
				backward := c.GetBackwardCachedSize()
				if forward < forwardMax {
					c.doCache(DirectionForward)
				} else if backward < backwardMax {
					c.doCache(DirectionBackward)
				} else {
					// there is nothing left to do so wait for thread to end
					<-c.stopCaching
					return
				}
			}
		}
	}(c)
}

// GetForwardCachedSize gets the total cache in the forward facing cache
func (c *Cache) GetForwardCachedSize() int64 {
	max, _ := c.calculateCacheMaxSize()
	var total int64
	total = 0
	for e := c.currentEntry; e != nil; e = c.getNext(e) {
		f := e.Value.(*File)
		f.mutex.Lock()
		if total+int64(len(f.Buffer)) >= max {
			f.mutex.Unlock()
			break
		}
		total += int64(len(f.Buffer))
		f.mutex.Unlock()
	}
	return total
}

// GetBackwardCachedSize gets the total cache in the backward facing cache
func (c *Cache) GetBackwardCachedSize() int64 {
	_, max := c.calculateCacheMaxSize()
	var total int64
	total = 0
	for e := c.currentEntry; e != nil; e = c.getPrev(e) {
		f := e.Value.(*File)
		f.mutex.Lock()
		if total+int64(len(f.Buffer)) >= max {
			f.mutex.Unlock()
			break
		}
		total += int64(len(f.Buffer))
		f.mutex.Unlock()
	}
	return total
}

// GetTotalCachedSize returns the number of bytes that have been cached
func (c *Cache) GetTotalCachedSize() int64 {
	var total int64
	total = 0
	for e := c.entries.Front(); e != nil; e = e.Next() {
		f := e.Value.(*File)
		f.mutex.Lock()
		total += int64(len(f.Buffer))
		f.mutex.Unlock()
	}
	return total
}

// NewCache returns a new initialized Cache struct
func NewCache(maxSize int64, forwardPercentage float64) *Cache {
	c := &Cache{
		ForwardPercentage: forwardPercentage,
		MaxSize:           maxSize,
	}
	c.stopCaching = make(chan bool)
	return c
}
