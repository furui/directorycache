package directorycache

import "os"

// SortType is what to sort the file by
type SortType int

const (
	// SortTypeName sorts by name
	SortTypeName SortType = iota
	// SortTypeDate sorts by date
	SortTypeDate
	// SortTypeSize sorts by size
	SortTypeSize
)

// SortOrder is what direction to sort the file
type SortOrder int

const (
	// SortOrderAscending sorts in ascending order
	SortOrderAscending SortOrder = iota
	// SortOrderDescending sorts in descending order
	SortOrderDescending
)

// ByDate sorts directories by modification date
type ByDate []os.FileInfo

func (a ByDate) Len() int           { return len(a) }
func (a ByDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByDate) Less(i, j int) bool { return a[i].ModTime().Unix() < a[j].ModTime().Unix() }

// BySize sorts directories by size
type BySize []os.FileInfo

func (a BySize) Len() int           { return len(a) }
func (a BySize) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a BySize) Less(i, j int) bool { return a[i].Size() < a[j].Size() }
