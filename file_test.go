package directorycache

import "testing"

func TestFileRead(t *testing.T) {
	d, err := NewDirectory("testing/american", 10000, 0.8, true)
	if err != nil {
		t.Error(err)
	}
	defer d.Close()

	f := d.GetCurrentFile()
	buf := make([]byte, 10)
	readBytes, err := f.Read(buf)
	if err != nil {
		t.Error(err)
	}
	if readBytes != 10 {
		t.Error("Expected 10 bytes but got: ", readBytes)
	}
	if buf[0] != '-' {
		t.Error("Unexpected character found: ", buf[0])
	}

	if d.Cache.GetForwardCachedSize() < 1 {
		t.Error("Forward cache is less than 1: ", d.Cache.GetForwardCachedSize())
	}
}
