package directorycache

import (
	"container/list"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
)

// Direction is the direction the user is going down the list
type Direction int

const (
	// DirectionForward states the user is going forward down the list
	DirectionForward Direction = iota
	// DirectionBackward states the user is going backwards up the list
	DirectionBackward
)

// Directory stores the base structure
type Directory struct {
	Path      string
	SortType  SortType
	SortOrder SortOrder
	circular  bool

	Cache *Cache

	direction Direction

	files       *list.List
	currentFile *list.Element
}

// GetCurrentFile returns the currently selected file
func (d *Directory) GetCurrentFile() *File {
	if d.files.Len() > 0 && d.currentFile != nil {
		return d.currentFile.Value.(*File)
	}
	return nil
}

func (d *Directory) setCurrentFile(f *list.Element) {
	d.currentFile = f
	d.Cache.SetCurrentEntry(f)
}

func (d *Directory) setDirection(direction Direction) {
	d.direction = direction
	d.Cache.SetDirection(direction)
}

// NextFile increments to the next file in the list and returns it
func (d *Directory) NextFile() *list.Element {
	if d.currentFile == nil || d.files.Len() < 1 {
		return nil
	}
	next := d.currentFile.Next()
	d.setDirection(DirectionForward)
	if next == nil {
		if d.circular == true {
			d.setCurrentFile(d.files.Front())
			return d.currentFile
		}
		return nil
	}
	d.setCurrentFile(next)
	return d.currentFile
}

// PrevFile increments to the previous file in the list and returns it
func (d *Directory) PrevFile() *list.Element {
	if d.currentFile == nil || d.files.Len() < 1 {
		return nil
	}
	next := d.currentFile.Prev()
	d.setDirection(DirectionBackward)
	if next == nil {
		if d.circular == true {
			d.setCurrentFile(d.files.Back())
			return d.currentFile
		}
		return nil
	}
	d.setCurrentFile(next)
	return d.currentFile
}

func (d *Directory) parsePath() (string, string, error) {
	var directory string
	var filename string

	f, err := os.Stat(d.Path)
	if err != nil {
		return "", "", err
	}

	if f.IsDir() {
		directory = filepath.Clean(d.Path)
		filename = ""
	} else {
		directory, filename = filepath.Split(filepath.Clean(d.Path))
	}

	return directory, filename, nil
}

// ReadDir reads the directory and initializes the cache
func (d *Directory) ReadDir() error {
	// first let's stat the path and figure out if we have a directory or a file
	directory, filename, err := d.parsePath()
	if err != nil {
		return err
	}

	// get a list of files in this directory
	d.files = list.New()
	files, err := ioutil.ReadDir(directory)
	if err != nil {
		return err
	}

	// sort listing
	if d.SortType == SortTypeName {
		if d.SortOrder == SortOrderDescending {
			for i := len(files)/2 - 1; i >= 0; i-- {
				opp := len(files) - 1 - i
				files[i], files[opp] = files[opp], files[i]
			}
		}
	} else if d.SortType == SortTypeDate {
		if d.SortOrder == SortOrderAscending {
			sort.Sort(ByDate(files))
		} else {
			sort.Sort(sort.Reverse(ByDate(files)))
		}
	} else if d.SortType == SortTypeSize {
		if d.SortOrder == SortOrderAscending {
			sort.Sort(BySize(files))
		} else {
			sort.Sort(sort.Reverse(BySize(files)))
		}
	}

	// populate list
	for _, file := range files {
		if file.IsDir() == false {
			f := new(File)
			f.Info = file
			f.Path = filepath.Join(directory, f.Info.Name())
			f.readPointer = 0
			f.State = EntryStateCaching
			f.Buffer = []byte{}
			e := d.files.PushBack(f)
			if file.Name() == filename {
				defer d.setCurrentFile(e)
			}
		}
	}

	d.Cache.SetEntries(d.files)

	// if no filename was chosen, pick the first one
	if d.files.Len() > 0 {
		if filename == "" {
			d.setCurrentFile(d.files.Front())
		}
	} else {
		d.setCurrentFile(nil)
	}

	return nil
}

// SetCircular sets if the directory listing is circular
func (d *Directory) SetCircular(circular bool) {
	d.circular = circular
	d.Cache.SetCircular(circular)
}

// NewDirectory creates a new Directory and returns a pointer to it
func NewDirectory(path string, maxSize int64, forwardPercentage float64, circular bool) (*Directory, error) {
	d := new(Directory)
	d.Path = path
	d.circular = circular
	d.Cache = NewCache(maxSize, forwardPercentage)
	d.SetCircular(circular)

	err := d.ReadDir()
	if err != nil {
		return nil, err
	}

	return d, nil
}

// Close cleans up and prepares for exit
func (d *Directory) Close() {
	d.Cache.StopCaching()
}
