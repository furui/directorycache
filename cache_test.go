package directorycache

import (
	"testing"
)

func TestCaching(t *testing.T) {
	d, err := NewDirectory("testing/american", 10000, 0.8, true)
	if err != nil {
		t.Error(err)
	}
	defer d.Close()

	f := d.GetCurrentFile()
	if f.State == EntryStateNotCached {
		t.Error("Found EntryStateNotCached, but expected different value")
	}

}
